﻿using UnityEngine;
using System.Collections;

public class camerafollow : MonoBehaviour {
	public Transform player;
	private Vector3 currentpos;

	void Start(){
		currentpos = transform.position;
	}
	// Update is called once per frame
	void Update () {

		transform.position = currentpos + player.position;
	}
}
